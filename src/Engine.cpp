#include "Engine.h"

BK::Game* BK::Engine::game = NULL;
vector<BK::ClipData*> BK::Engine::textures;

int BK::Engine::RunGame()
{
    if(!Engine::game)
        return GAME_NOT_FOUND;
    if(Engine::game->running)
        return GAME_ALREADY_RUNNING;

    return Engine::game->Run();
}

BK::Object* BK::Engine::Get(string name, Object* obj)
{
    if(name == "ENGINE")
        return Engine::game;

    if(!obj)
        return NULL;

    Object* tmp = NULL;

    if(obj->GetName() == name)
        return obj;

    for(unsigned int i = 0; (obj->children ? i < obj->children->size() : false); i++)
    {
        tmp = Engine::Get(name, (*obj->children)[i]);

        if(tmp)
            return tmp;

        tmp = NULL;
    }

    return NULL;
}

vector<BK::Object*> BK::Engine::Get(BK::ObjectType type, BK::Object* obj)
{
    vector<Object*> v;

    if(!obj)
        return {};

    if(obj->GetObjectType() == type)
        v.push_back(obj);

    for(unsigned int i = 0; (obj->children ? i < obj->children->size() : false); i++)
    {
        vector<Object*> tmp = Engine::Get(type, (*obj->children)[i]);

        v.insert(v.end(), tmp.begin(), tmp.end());
    }

    return v;
}


BK::Game* BK::Engine::CreateGame(string name)
{
    Engine::game = new Game(name);
    return Engine::game;
}



void BK::Engine::StopGame()
{
    Engine::game->running = false;
    delete Engine::game;
    Engine::game = NULL;
}
