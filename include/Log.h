#ifndef BKLOG_H
#define BKLOG_H

#include <iostream>
#include <string>
#include "LogLevels.h"

using namespace std;

class BKLog
{
    public:
        static void Init(string filename);
        static void AddEntry(string classname, string methodname, string errorMessage, LogLevel level);
    protected:
    private:
        static ofstream file;
};

#endif // BKLOG_H
