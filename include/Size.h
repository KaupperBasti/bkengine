#ifndef SIZE_H_INCLUDED
#define SIZE_H_INCLUDED
namespace BK
{
    struct Size
    {
        float w;
        float h;
    };
}

#endif // SIZE_H_INCLUDED
