#include "Game.h"
#include "Engine.h"

BK::Game::Game(string name) : Object(name)
{
    png_init(0,0);

    Game::SetObjectType(GAME_OBJECT);
    Game::running = false;
    Game::initialized = false;

    Game::windowWidth = 0;
    Game::windowHeight = 0;
    Game::windowTitle = "";

}

BK::Game::~Game()
{
    Game::initialized = false;
}



BK::Error BK::Game::Init(int width, int height, string title)
{
    Game::windowWidth = width;
    Game::windowHeight = height;
    Game::windowTitle = title;

    glutInit(new int(0),NULL);

	glutInitContextVersion( 2, 1 );

	//Create Double Buffered Window
	glutInitDisplayMode( GLUT_DOUBLE );
	glutInitWindowSize( width, height);
	glutCreateWindow( title.c_str() );

    glViewport( 0.f, 0.f, width, height);


    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho( 0.0, width, height, 0.0, -1.0, 1.0 );

    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();

    glClearColor( 1.f, 1.f, 1.f, 1.f );

    glEnable( GL_TEXTURE_2D );
    glEnable( GL_LINE_SMOOTH );
    glEnable( GL_BLEND );

    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );


    if( glGetError()!= GL_NO_ERROR )
        return OPENGL_INIT_FAILED;

    Event::Init();

    Game::initialized = true;

    return SUCCESS;
}



BK::Error BK::Game::Run()
{
    if(Game::running)
        return GAME_ALREADY_RUNNING;
    if(!Game::initialized && Game::Init() != SUCCESS)
        return FAILED_TO_START_GAME;


    Game::running = true;

	glutDisplayFunc(&Game::DisplayFunc);
	glutIdleFunc(&Game::DisplayFunc);

	glutTimerFunc( 0, &Game::Tick, 1 );
	glutMainLoop();

    return SUCCESS;
}

void BK::Game::DisplayFunc()
{
    glClear(GL_COLOR_BUFFER_BIT);

    Engine::game->Render();

    glutSwapBuffers();
}

void BK::Game::Tick(int var)
{
    static Timer t;
    t.Stop();
    t.Start();


    DisplayFunc();

    while(Event::HasEvent())
        Engine::game->Event(Event::PollEvent(), var);
    Engine::game->Loop(var);


    t.Pause();

    if(Engine::game->running)
        glutTimerFunc( 0, &Game::Tick, t.GetTicks() );
}


void BK::Game::Quit()
{
    Game::running = false;
    delete this;
}





BK::Error BK::Game::ActivateScene(string name)
{
    Object* obj = GetChild(name);
    if(!obj)
        return CHILD_OBJECT_NOT_FOUND;
    DeactivateChildren();
    obj->Activate();
    return SUCCESS;
}
