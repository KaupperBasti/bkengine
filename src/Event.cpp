#include "Event.h"


map<BK::MouseButton, bool> BK::Event::mousePressStates;
map<BK::KeyboardKey, bool> BK::Event::keyboardPressStates;
queue<BK::EventData*> BK::Event::eventQueue;

void BK::Event::Init()
{
    glutKeyboardFunc(BK::Event::KeyPressed);
    glutKeyboardUpFunc(BK::Event::KeyReleased);
    glutMouseFunc(BK::Event::MouseEvent);
    glutSpecialFunc(BK::Event::SpecialPressed);
    glutSpecialUpFunc(BK::Event::SpecialRelease);

    glutReshapeFunc([&](int x, int y){ });
}

BK::EventData* BK::Event::PollEvent()
{
    EventData* ed;

    if(Event::eventQueue.empty())
        return NULL;
    ed = eventQueue.front();
    eventQueue.pop();
    return ed;
}

//Translate GL KeyCodes to BK KeyCodes
BK::KeyboardKey BK::Event::IntToKey(int key)
{
    // F1 - F12
    if(key <= 0x0C)
        return (KeyboardKey)(key + 0x6F);


    // GLUT_KEY_LEFT - GLUT_KEY_INSERT
    if(key >= 0x84 && key <= 0x8C)
        return (KeyboardKey)(key - 0x20);


    //
    // ASCII
    //

    // 0 - 9
    if(key >= 0x30 && key <= 0x39)
        return (KeyboardKey)key;

    // A - Z
    if(key >= 0x61 && key <= 0x7A)
        return (KeyboardKey)(key - 0x20);

    // * + / - , .
    if(key >= 0x2A && key <= 0x2E)
        return (KeyboardKey)key;

    //
    // END OF ASCII
    //

    // GLUT_KEY_SHIFT_L - GLUT_KEY_ALT_R
    if(key >= 0x90 && key <= 0x95)
        return (KeyboardKey)(key - 0x0D);


    // SPECIAL KEYS
    switch(key)
    {
    case KEY_BACKSPACE:
        return (KeyboardKey)0x08;
    case KEY_TAB:
        return (KeyboardKey)0x09;
    case KEY_ESCAPE:
        return (KeyboardKey)0x1B;
    case KEY_SPACE:
        return (KeyboardKey)0x20;
    case KEY_ENTER:
        return (KeyboardKey)0x0D;
    case KEY_DELETE:
        return (KeyboardKey)0x7F;
    }

    return NO_KEY;
}

void BK::Event::SpecialPressed (int key, int x, int y)
{
    KeyboardKey k = IntToKey((key >= 0x64 && key <= 0x95) ? key + 0x20 : key);
    ChangeKeyboardKeyState(k, true);
    eventQueue.push(new EventData(k, x, y, true));
}

void BK::Event::SpecialRelease (int key, int x, int y)
{
    KeyboardKey k = IntToKey((key >= 0x64 && key <= 0x95) ? key + 0x20 : key);
    ChangeKeyboardKeyState(k, false);
    eventQueue.push(new EventData(k, x, y, false));
}

void BK::Event::KeyPressed (unsigned char key, int x, int y)
{
    KeyboardKey k = IntToKey(key);
    ChangeKeyboardKeyState(k, true);
    eventQueue.push(new EventData(k, x, y, true));
}

void BK::Event::KeyReleased (unsigned char key, int x, int y)
{
    KeyboardKey k = IntToKey(key);
    ChangeKeyboardKeyState(k, false);
    eventQueue.push(new EventData(k, x, y, false));
}


void BK::Event::MouseEvent (int button, int state, int x, int y)
{
    MouseButton b = (MouseButton)button;
    ChangeMouseButtonState(b, (state == GLUT_UP ? false : true));
    eventQueue.push(new EventData(b, x, y, (state == GLUT_UP ? false : true)));
}


void BK::Event::ChangeKeyboardKeyState(KeyboardKey key, bool pressed)
{
    if(Event::keyboardPressStates.empty() || Event::keyboardPressStates.count(key) == 0)
        Event::keyboardPressStates.insert(pair<KeyboardKey, bool>(key, pressed));
    else
        Event::keyboardPressStates[key] = pressed;
}

void BK::Event::ChangeMouseButtonState(MouseButton button, bool pressed)
{
    if(Event::mousePressStates.empty() || Event::mousePressStates.count(button) == 0)
        Event::mousePressStates.insert(pair<MouseButton, bool>(button, pressed));
    else
        Event::mousePressStates[button] = pressed;
}


bool BK::Event::IsPressed(MouseButton button)
{
    if(Event::mousePressStates.empty() || Event::mousePressStates.count(button) == 0)
        return false;
    return Event::mousePressStates[button];
}

bool BK::Event::IsPressed(KeyboardKey key)
{
    if(Event::keyboardPressStates.empty() || Event::keyboardPressStates.count(key) == 0)
        return false;
    return Event::keyboardPressStates[key];
}


bool BK::Event::HasEvent()
{
    return !eventQueue.empty();
}
