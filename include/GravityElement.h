#ifndef GRAVITYELEMENT_H
#define GRAVITYELEMENT_H

#include "Element.h"

using namespace BK;

class GravityElement : public Element
{
    public:
        GravityElement(string name, Object* parent) : Element(name, parent){}
        Error Loop(float);
        virtual bool OnCollision(Element* other, Point* startPos, Point* endPos);
        virtual ~GravityElement();
    protected:
    private:
        Timer timer;
};

#endif // GRAVITYELEMENT_H
