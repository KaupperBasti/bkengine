/**
    Clip.h
    Purpose:
        Represents a Clip

    @author Sebastian Kaupper
    @version 1.0
    @date 04.07.2014
*/

#ifndef CLIP_H
#define CLIP_H

#include "Engine.h"
#include "Object.h"
#include "Color.h"
#include "FlipState.h"
#include "pnglite.h"
#include <fstream>


#include "GL/freeglut.h"
#include "GL/freeglut_ext.h"
#include "GL/freeglut_std.h"
#include "GL/glut.h"
#include "GL/gl.h"

using namespace std;
namespace BK
{
    class Clip final : public Object
    {
        friend class Engine;
        public:
            Error Init(string name, string path, Rect srcrect = {-1,-1,-1,-1});
            Error Init(string textureName);

            Clip(string name, Object* parent);
        protected:
        private:
            virtual ~Clip();
            Error Render();
            Error ResetElementSize();

            Error LoadImage(string name, string path, Rect srcrect);

            GLuint id;
    };
}
#endif // CLIP_H
