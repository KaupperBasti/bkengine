/**
    Scene.h
    Purpose:
        Represents a Scene which can contain Elements.

    @author Sebastian Kaupper
    @version 1.0
    @date 04.07.2014
*/


#ifndef SCENE_H
#define SCENE_H

#include "Element.h"
#include "Object.h"
#include "Game.h"

#include <algorithm>
#include <map>
#include <iostream>
namespace BK
{
    class Element;
    class Scene final : public Object
    {
        friend class Element;
        public:
            Scene(string name, Object* parent);

            Error RefreshElementMap(Element*, int nzindex);
            void printMap();

            Error Init();
            virtual Error Render();
            Error Loop(float frametime);
            Error Event(BK::EventData* event, float frametime);
        protected:
            map<int, vector<Element*> > elementsInScene;
        private:

    };
}
#endif // SCENE_H
