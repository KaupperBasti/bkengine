#ifndef BKFLIPSTATE_H
#define BKFLIPSTATE_H

namespace BK
{
    enum FlipState
    {
        FLIP_NONE = 0,
        FLIP_HORIZONTAL = 1,
        FLIP_VERTICAL = 2
    };
}
#endif // BKFLIPSTATE_H
