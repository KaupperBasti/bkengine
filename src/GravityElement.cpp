#include "GravityElement.h"

Timer timer;

Error GravityElement::Loop(float time)
{
    timer.Start();
   //Move(0, timer.GetTicks() / 300);
    return Element::Loop(time);
}

bool GravityElement::OnCollision(Element* other, Point* startPos, Point* endPos)
{
    if(startPos->y - endPos->y < 0.f)
        timer.Stop();
    return false;
}


GravityElement::~GravityElement()
{
    //dtor
}
