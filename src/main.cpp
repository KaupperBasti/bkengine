#include <iostream>
#include "Engine.h"
#include "Player.h"
#include "Wall.h"
#include <sstream>
#include "pnglite.h"

using namespace BK;
int main(int argc, char* argv[])
{
    setvbuf(stdout, NULL, _IONBF, 0);
    cout<<"seas"<<endl;
    Engine::CreateGame("game")->Init(800,600,"Maxl (better than Berthold)");
    Engine::Create<Scene>("scene", Engine::Get())->Init();
    Engine::Create<Player>("maxl", Engine::Get("scene"))->Init(Point{50, 50}, 2, Size{300,300});
    Engine::Create<Animation>("walking", Engine::Get("maxl"))->Init(20);
    Engine::Create<Element>("jo", Engine::Get("maxl"))->Init(Point{110,-20}, 0, Size{-1,-1}, RELATIVE_LOCATION);
    Engine::Create<Clip>("jobild", Engine::Get("jo"))->Init("jo", "test.png");

    Engine::Create<Animation>("standing", Engine::Get("maxl"))->Init(1000000);
    Engine::Create<Clip>("standingclip", Engine::Get("standing"))->Init("standing", "walking.png", {900,0,300,300});

    Player* player = (Player*)Engine::Get("maxl");
    player->ActivateAnimation("standing");
    stringstream ss;
    for(int i = 0; i < 6; i++)
    {
        ss.str("walkClip"); ss<<i;
        string s = ss.str();

       Clip* clip = Engine::Create<Clip>(s, Engine::Get("walking"));

       clip->Init(s, "walking.png", {1500 - 300 * i, 0, 300, 300});
    }


    // 55 x 48

    Engine::Create<Element>("walls", Engine::Get("scene"))->Init(Point{0,600-25}, 0, Size{800, 25});
    Engine::Create<Clip>("bricktemplate", Engine::Get("walls"))->Init("brick", "brick.png");

    for(int i = 0; i < 32; i++)
    {
        ss.str("brickele_floor");
        ss<<i;
        Engine::Create<Element>(ss.str(), Engine::Get("walls"))->Init({i * 25, 600-25}, 0, {25,25});
        Engine::Create<Clip>("brick", Engine::Get(ss.str()))->Init("brick");
    }

    for(int i = 0; i < 24; i++)
    {
        ss.str("brickele_leftwall");
        ss<<i;
        Engine::Create<Element>(ss.str(), Engine::Get("walls"))->Init({0, i * 25}, 0, {25,25});
        Engine::Create<Clip>("brick", Engine::Get(ss.str()))->Init("brick");
    }

    for(int i = 0; i < 24; i++)
    {
        ss.str("brickele_rightwall");
        ss<<i;
        Engine::Create<Element>(ss.str(), Engine::Get("walls"))->Init({800 - 25, i * 25}, 0, {25,25});
        Engine::Create<Clip>("brick", Engine::Get(ss.str()))->Init("brick");
    }

    for(int i = 0; i < 32; i++)
    {
        ss.str("brickele_ceiling");
        ss<<i;
        Engine::Create<Element>(ss.str(), Engine::Get("walls"))->Init({i * 25, 0}, 0, {25,25});
        Engine::Create<Clip>("brick", Engine::Get(ss.str()))->Init("brick");
    }

    Engine::RunGame();

    return 0;
}
/*
    cout << "sers" << endl;
    Player* player = new Player("player", Engine::Get(game, "scene"));
    player->SetRenderState(RENDERSTATE_RENDER);
    player->Init(new Point{50,50}, new Size{100,-1}, 0, FIXED_LOCATION);

    Animation* walking = new Animation("walking", player);
    walking->Init(50);
    stringstream ss;
    for(int i = 0; i < 6; i++)
    {
        ss.str("walking");
        ss<<i;
        string s = ss.str();
        (new Clip(s, walking))->Init(s, "walking.png", new Rect{i*300,0,300,300});
    }

    (new Wall("floor", Engine::Get(game, "scene")))->Init(new Point{0,580}, new Size{800,20}, 0, FIXED_LOCATION);
    Engine::Get(game, "floor")->SetRenderState(RENDERSTATE_RENDER);
    (new Clip("jo", Engine::Get(game, "floor")))->Init("jo", "test.png", NULL);

    (new Wall("iwas", Engine::Get(game, "scene")))->Init(new Point{0,0}, new Size{800,20}, 0, FIXED_LOCATION);
    Engine::Get(game, "iwas")->SetRenderState(RENDERSTATE_RENDER);
    (new Clip("jo", Engine::Get(game, "iwas")))->Init("jo", "test.png", NULL);

    (new Wall("wall", Engine::Get(game, "scene")))->Init(new Point{780,0}, new Size{20,600}, 0, FIXED_LOCATION);
    Engine::Get(game, "wall")->SetRenderState(RENDERSTATE_RENDER);
    (new Clip("jo", Engine::Get(game, "wall")))->Init("jo", "test.png", NULL);

    (new Wall("left", Engine::Get(game, "scene")))->Init(new Point{0,0}, new Size{20,600}, 0, FIXED_LOCATION);
    Engine::Get(game, "left")->SetRenderState(RENDERSTATE_RENDER);
    (new Clip("jo", Engine::Get(game, "left")))->Init("jo", "test.png", NULL);

    (new Wall("dd", Engine::Get(game, "scene")))->Init(new Point{0,400}, new Size{200,20}, 0, FIXED_LOCATION);
    Engine::Get(game, "dd")->SetRenderState(RENDERSTATE_RENDER);
    (new Clip("jo", Engine::Get(game, "dd")))->Init("jo", "test.png", NULL);

    (new Wall("ff", Engine::Get(game, "scene")))->Init(new Point{600,400}, new Size{200,20}, 0, FIXED_LOCATION);
    Engine::Get(game, "ff")->SetRenderState(RENDERSTATE_RENDER);
    (new Clip("jo", Engine::Get(game, "ff")))->Init("jo", "test.png", NULL);



    game->Run();

    return 0;

}
*/
//    Engine::StartEngine();
//
//
//    Game* game = Engine::CreateGame("game");
//    game->Init(800,600,"BK TEST WINDOW");
//    cout<<game->LoadImage("jo", "test.png")<<endl;
//    //cout<<game->LoadImage("cw", "castlewars.png")<<endl;
//
//    Engine::Create<Scene>("scene2", game)->Init();
//    Engine::Create<Scene>("scene1", game)->Init();
//    Engine::Create<Element>("e1", Engine::Get(game, "scene1"))->Init(new Point{400, 200}, new Size{400, 300}, 3, NULL, FIXED_LOCATION);
//    Engine::Create<Element>("e2", Engine::Get(game, "scene1"))->Init(new Point{0, 0}, new Size{300, 300}, 2, NULL, FIXED_LOCATION);
//    Engine::Create<Element>("p1", Engine::Get(game, "scene2"))->Init(new Point{0, 200}, new Size{200, 300}, 6, NULL, FIXED_LOCATION);
//    //Engine::Create<Player>("p1", Engine::Get(game, "scene2"))->Init(new Point{400, 200}, new Size{100, 300}, 2, NULL, FIXED_LOCATION);
//    Animation* a1  = Engine::Create<Animation>("a1", Engine::Get(game, "p1"));
//    a1->Init(1000);
//    Engine::Create<Animation>("a2", Engine::Get(game, "e2"))->Init(1000);
//    Engine::Create<Clip>("jo", Engine::Get(game, "a1"))->Init("jo");
//    Engine::Create<Clip>("cw", Engine::Get(game, "a2"))->Init("cw");
//    Engine::Create<Clip>("jo2", Engine::Get(game, "a2"))->Init("jo");
//
//    Engine::RunGame(game,false);
//
//    Engine::StopEngine();


