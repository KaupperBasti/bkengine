#ifndef BKTimer_H
#define BKTimer_H

#include "Engine.h"
namespace BK
{
    class Timer
    {
        public:
            Timer();
            virtual ~Timer();

            /**
            Starts the Timer (i.e. sets the ticks to zero). Does nothing if the Timer is already started
            @return 1
            */
            int Start();

            /**
            Pauses the Timer. Ticks will temporarily stop being counted.
            @return 1
            */
            int Pause();

            /**
            Unpauses the Timer. Does nothing if the Timer is not running or not paused.
            @return 0
            */
            int Unpause();

            /**
            Stops the Timer and resets everything to 0.
            @return 0
            */
            int Stop();

            /**
            Gets the ticks elapsed since the Timer sarted.
            @return The ticks since the Timer started
            */
            int GetTicks();

            /**
            @return Whether the Timer is started (pause status does NOT matter)
            */
            bool IsStarted();

            /**
            @return Whether the Timer is paused
            */
            bool IsPaused();
        protected:
        private:
            bool started;
            bool paused;
            int startTicks;
            int pauseTicks;
    };
}
#endif // BKTimer_H
