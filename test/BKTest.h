#ifndef BKTEST_H_INCLUDED
#define BKTEST_H_INCLUDED

#include "../include/Object.h"
#include <iostream>

class BKTest : public Object{
public:
    BKTest(string name) : Object(name){}


    virtual Error Render()
    {
//            cout<<"Name: "<<this->GetName()<<endl;

            return Object::Render();
    }
};

#endif // BKTEST_H_INCLUDED
