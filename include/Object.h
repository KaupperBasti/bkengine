#ifndef BKObject_H
#define BKObject_H

#include <string>
#include <vector>
using namespace std;

#include "Error.h"
#include "RenderStates.h"
#include "ObjectTypes.h"
#include "Event.h"
namespace BK
{
    class Object
    {
        friend class Engine;

        public:
            Object(string name);
            Object(string name, Object*);
            Object(string name, Object*, vector<Object*>);
            Object(string name, vector<Object*>);

            virtual ~Object() = 0;


            bool IsInitialized() { return Object::initialized; }

            void SetRenderState(RenderState);
            RenderState GetRenderState();

            void SetObjectType(ObjectType);
            ObjectType GetObjectType();


            string GetName();
            Object* GetChild(string);
            Object* GetChild(int);
            Object* GetParent();


            virtual bool AllowChild(Object*);
            bool HasChild(Object*);


            Object* ChangeParent(Object*);

            Object* GetFirstParent(ObjectType);

            Error AddChild(Object*);
            Error AddChild(Object*, RenderState);
            Error AddChildren(vector<Object*>);
            Error AddChildren(vector<Object*>, RenderState);
            Error AddChildren(vector<Object*>, vector<RenderState>);


            Error RemoveChild(Object*);
            Error RemoveChild(string);

            Error DeleteChild(Object*);
            Error DeleteChild(string);

            Error ClearChildren();

            bool IsActive() { return renderState == RENDERSTATE_RENDER; }

            void Activate();
            void Deactivate();

            Error ActivateChild(string);
            Error ActivateChildren();
            Error ActivateChildren(vector<string>);

            Error DeactivateChild(string);
            Error DeactivateChildren();
            Error DeactivateChildren(vector<string>);

            Error ToggleChild(string);
            Error ToggleChildren();
            Error ToggleChildren(vector<string>);


            virtual Error Render(void);
            virtual Error Loop(float);
            virtual Error Event(BK::EventData*, float);

            virtual void ChildAdded(Object* object){}

        protected:
            Object* parent;
            vector<Object*>* children;

            string name;
            BK::RenderState renderState;
            ObjectType objectType;

            bool initialized;







        public:
            //DEBUG methods
            string GetChildNames()
            {
                string s = "Parent:\t";
                s.append(this->GetName());
                s.append("\r\n");

                if(!children)
                    return s;

                for(unsigned int i = 0; i < children->size(); i++)
                {
                    s.append("Child:\t");
                    s.append((*children)[i]->GetName());
                    s.append("\r\n");
                }
                return s;
            }

            string GetRenderStates()
            {
                string s = "";

                for(unsigned int i = 0; i < children->size(); i++)
                {
                    s.append((*children)[i]->GetName());
                    s.append(":\t");
                    s.append((*children)[i]->GetRenderState() == RENDERSTATE_RENDER ? "RENDERSTATE_RENDER" : "RENDERSTATE_IGNORE");
                    s.append("\r\n");
                }
                return s;
            }
    };
}
#endif // Object_H
