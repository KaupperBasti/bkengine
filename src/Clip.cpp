#include "Clip.h"

BK::Clip::Clip(string name, Object* parent) : Object(name, parent)
{
    Clip::SetObjectType(CLIP_OBJECT);
    Clip::id = 0;
}


BK::Clip::~Clip()
{
}

BK::Error BK::Clip::Init(string textureName)
{
    for(int i = 0; i < Engine::textures.size(); i++)
        if(Engine::textures[i]->name == textureName)
        {
            Clip::id = Engine::textures[i]->textureID;
            return SUCCESS;
        }

    return TEXTURE_DOES_NOT_EXIST;
}

BK::Error BK::Clip::Init(string name, string path, Rect srcrect)
{

    return LoadImage(name, path, srcrect);
}

BK::Error BK::Clip::LoadImage(string name, string filename, Rect clip)
{
    int r;
    png_t pt;
    r = png_open_file(&pt, filename.c_str());

    png_print_info(&pt);

    if(clip.w == -1 && clip.h == -1)
    {
        clip = Rect{0,0, pt.width, pt.height};
    }
    unsigned char* data = (unsigned char*)malloc(pt.width*pt.height*pt.bpp);
    GLuint* pixels = (GLuint*)malloc(clip.w*clip.h*pt.bpp);
    r = png_get_data(&pt, data);
    unsigned int j = 0;
    unsigned int i = 0;
    for(; i < pt.width*pt.height*pt.bpp; i+=4)
    {
        if(i/4 % pt.width >= clip.x && i/4 % pt.width < clip.x + clip.w )
        {
            if(i/4 / pt.width >= clip.y && i/4 / pt.width < clip.y + clip.h)
            {
                pixels[j++] = (GLuint)((data[i] << 0) | (data[i+1] << 8) | (data[i+2] << 16) | (data[i+3] << 24));
            }
        }
    }
    //Generate texture ID
    glGenTextures( 1, &id);

    //Bind texture ID
    glBindTexture( GL_TEXTURE_2D, id );

    //Generate texture
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, clip.w, clip.h, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels );
    //free(pixels);

    //free(data);
    //Set texture parameters
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );

    //Unbind texture
    glBindTexture(GL_TEXTURE_2D, NULL);

    ClipData* d = new ClipData{name, id, clip.w, clip.h};

    Engine::textures.push_back(d);
    ResetElementSize();

    return SUCCESS;
}

BK::Error BK::Clip::ResetElementSize()
{
    int w = 0;
    int h = 0;

    for(int i = 0; i < Engine::textures.size(); i++)
        if(Engine::textures[i]->textureID == id)
        {
            w = Engine::textures[i]->width;
            h = Engine::textures[i]->height;
        }

    Element* element = ((Element*)GetFirstParent(ELEMENT_OBJECT));
    if(!element)
        return OBJECT_NOT_FOUND;

    Size* elesize = (element->GetSize());

    if(elesize->w <= 0 && elesize->h <= 0)
    {
        elesize->w = (float)w;
        elesize->h = (float)h;
    }
    else if(elesize->w <= 0)
        elesize->w = (float)w / (float)h * elesize->h;
    else if(elesize->h <= 0)
        elesize->h = (float)h / (float)w * elesize->w;

    return SUCCESS;
}


BK::Error BK::Clip::Render()
{
    Element* element = ((Element*)GetFirstParent(ELEMENT_OBJECT));
    if(!element)
        return OBJECT_NOT_FOUND;

    FlipState flip = ((Element*)GetFirstParent(ELEMENT_OBJECT))->GetFlipState();
    float opacity = ((Element*)GetFirstParent(ELEMENT_OBJECT))->GetOpacity();

    bool horizontal = (((int)flip)%2) == 1;
    bool vertical = (((int)flip)>>1) == 1;

    Point* p = element->GetLocation();
    Size* size = element->GetSize();

    Rect renderRect = Rect{(int)p->x, (int)p->y, (int)size->w, (int)size->h};


    glLoadIdentity();

    glBindTexture( GL_TEXTURE_2D, id );
    glTranslatef( renderRect.x, renderRect.y, 0 );

    if(element->rotation != 0 || vertical || horizontal)
    {
        glTranslatef(renderRect.w / 2, renderRect.h / 2, 0);
        if(element->rotation != 0)
            glRotatef(element->rotation, 0.0, 0.0, 1.0);
        if(vertical)
            glRotatef(180, 0.0,1.f,0.0);
        if(horizontal)
            glRotatef(180,1.f ,0.0,0.0);
        glTranslatef(-(renderRect.w / 2), -(renderRect.h / 2), 0);
    }

    glColor4f(1.f,1.f,1.f, opacity);
    glBegin( GL_QUADS );
        glTexCoord2f( 0.f, 0.f ); glVertex2f( 0.f,          0.f          );
        glTexCoord2f( 1.f, 0.f ); glVertex2f( renderRect.w, 0.f          );
        glTexCoord2f( 1.f, 1.f ); glVertex2f( renderRect.w, renderRect.h );
        glTexCoord2f( 0.f, 1.f ); glVertex2f( 0.f,          renderRect.h );
    glEnd();

    return SUCCESS;
}



