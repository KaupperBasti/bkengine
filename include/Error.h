#ifndef ERRORCODES_H
#define ERRORCODES_H

namespace BK
{
    enum Error{
        SUCCESS = 0,
        ENGINE_NOT_STARTED = 1,
        GAME_NOT_INITIALIZED = 2,
        GAME_ALREADY_RUNNING = 3,
        GAME_NOT_FOUND = 4,
        CHILD_OBJECT_NOT_FOUND = 5,
        NO_CHILDREN = 6,
        WRONG_RENDER_STATE = 7,
        SIZE_MISMATCH = 8,
        OBJECT_NOT_FOUND = 9,
        FAILED_TO_LOAD_SURFACE = 10,
        FAILED_TO_CREATE_TEXTURE = 11,
        FAILED_TO_LOAD_FONT = 12,
        FONT_DOES_NOT_EXIST = 13,
        OPENGL_INIT_FAILED = 14,
        TEXTURE_DOES_NOT_EXIST = 15,




        FAILED_TO_START_GAME = 32,
    };
}
#endif // ERRORCODES_H
