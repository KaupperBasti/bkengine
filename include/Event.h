#ifndef BKEVENT_H
#define BKEVENT_H


#include "GL/freeglut.h"
#include "GL/freeglut_ext.h"
#include "GL/freeglut_std.h"
#include "GL/glut.h"
#include "GL/gl.h"
#include <queue>
#include <map>

using namespace std;

namespace BK
{
    enum EventType
    {
        NO_TYPE = 0,
        KEYBOARD = 2,
        MOUSE = 1
    };

    enum KeyboardKey
	{
	    NO_KEY = 0x0,

		KEY_LEFT = 0x64,
		KEY_UP = 0x65,
		KEY_RIGHT = 0x66,
		KEY_DOWN = 0x67,
		KEY_PAGE_UP = 0x68,
		KEY_PAGE_DOWN = 0x69,
		KEY_HOME = 0x6A,
		KEY_END = 0x6B,
		KEY_INSERT = 0x6C,










        KEY_BACKSPACE = 0x08,
		KEY_TAB = 0x09,
		KEY_ESCAPE = 0x1B,
		KEY_SPACE = 0x20,
		KEY_ENTER = 0x0D,
		KEY_DELETE = 0x7F,


		KEY_ZERO = 0x30,
		KEY_ONE = 0x31,
		KEY_TWO = 0x32,
		KEY_THREE = 0x33,
		KEY_FOUR = 0x34,
		KEY_FIVE = 0x35,
		KEY_SIX = 0x36,
		KEY_SEVEN = 0x37,
		KEY_EIGHT = 0x38,
		KEY_NINE = 0x39,

		KEY_A = 0x41,
		KEY_B = 0x42,
		KEY_C = 0x43,
		KEY_D = 0x44,
		KEY_E = 0x45,
		KEY_F = 0x46,
		KEY_G = 0x47,
		KEY_H = 0x48,
		KEY_I = 0x49,
		KEY_J = 0x4A,
		KEY_K = 0x4B,
		KEY_L = 0x4C,
		KEY_M = 0x4D,
		KEY_N = 0x4E,
		KEY_O = 0x4F,
		KEY_P = 0x50,
		KEY_Q = 0x51,
		KEY_R = 0x52,
		KEY_S = 0x53,
		KEY_T = 0x54,
		KEY_U = 0x55,
		KEY_V = 0x56,
		KEY_W = 0x57,
		KEY_X = 0x58,
		KEY_Y = 0x59,
		KEY_Z = 0x5A,

		KEY_MULTIPLY = 0x2A,
		KEY_ADD = 0x2B,
		KEY_DIVIDE = 0x2F,
		KEY_SUBTRACT = 0x2D,
		KEY_SEPERATOR = 0x2C,
		KEY_DECIMAL = 0x2E,



		KEY_F1 = 0x70,
		KEY_F2 = 0x71,
		KEY_F3 = 0x72,
		KEY_F4 = 0x73,
		KEY_F5 = 0x74,
		KEY_F6 = 0x75,
		KEY_F7 = 0x76,
		KEY_F8 = 0x77,
		KEY_F9 = 0x78,
		KEY_F10 = 0x79,
		KEY_F11 = 0x7A,
		KEY_F12 = 0x7B,





		KEY_SHIFT_LEFT = 0x83,
		KEY_SHIFT_RIGHT = 0x84,
		KEY_CTRL_LEFT = 0x85,
		KEY_CTRL_RIGHT = 0x86,
		KEY_ALT_LEFT = 0x87,
		KEY_ALT_RIGHT = 0x88
	};

    enum MouseButton
    {
        BUTTON_NONE = 3,
        BUTTON_LEFT = 0,
        BUTTON_MIDDLE = 1,
        BUTTON_RIGHT = 2
    };

    struct EventData
    {
        EventType type;
        int x;
        int y;
        bool pressed;

        MouseButton mouseButton;
        KeyboardKey keyboardKey;

        EventData()
        {
            type = NO_TYPE;
            mouseButton = BUTTON_NONE;
            keyboardKey = NO_KEY;
            x = -1;
            y = -1;
            pressed = false;
        }

        EventData(KeyboardKey key, int x, int y,bool pressed):type(KEYBOARD), x(x), y(y),pressed(pressed), keyboardKey(key)
        {
            mouseButton = BUTTON_NONE;
        }

        EventData(MouseButton button,  int x, int y, bool pressed):type(MOUSE), x(x), y(y),pressed(pressed), mouseButton(button)
        {
            keyboardKey = NO_KEY;
        }
    };



    class Event
    {
        public:
            static void Init();
            static EventData* PollEvent();
            static bool HasEvent();
            static bool IsPressed(KeyboardKey);
            static bool IsPressed(MouseButton);
        protected:
        private:
            static void KeyPressed(unsigned char key, int x, int y);
            static void SpecialPressed(int key, int x, int y);
            static void KeyReleased (unsigned char key, int x, int y);
            static void SpecialRelease (int key, int x, int y);
            static void MouseEvent (int button, int state, int x, int y);

            static void ChangeMouseButtonState(MouseButton, bool);
            static void ChangeKeyboardKeyState(KeyboardKey, bool);

            static KeyboardKey IntToKey(int key);




            static map<KeyboardKey, bool> keyboardPressStates;
            static map<MouseButton, bool> mousePressStates;
            static queue<EventData*> eventQueue;
    };
}
#endif // BKEVENT_H
