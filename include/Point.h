/**
    Point.h
    Purpose: Represents a point in 2D space with layered z-index.

    @author Christoph B�hmwalder
    @version 1.0
    @date 05.07.2014
*/


#ifndef POINT_H
#define POINT_H
namespace BK
{
    struct Point{
        float x;
        float y;
    };
}
#endif // POINT_H
