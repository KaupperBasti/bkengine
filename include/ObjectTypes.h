#ifndef OBJECTTYPES_H_INCLUDED
#define OBJECTTYPES_H_INCLUDED
namespace BK
{
    enum ObjectType{
        GAME_OBJECT=1,
        SCENE_OBJECT=2,
        ELEMENT_OBJECT=4,
        ANIMATION_OBJECT=8,
        CLIP_OBJECT=16,
    };
}
#endif // OBJECTTYPES_H_INCLUDED
