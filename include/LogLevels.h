#ifndef LOGLEVELS_H_INCLUDED
#define LOGLEVELS_H_INCLUDED

enum LogLevel
{
    TRACE = 1,
    INFO = 2,
    WARNING = 4,
    ERROR = 8,
    FATAL = 16
};

#endif // LOGLEVELS_H_INCLUDED
