#include "Timer.h"

BK::Timer::Timer()
{
    //ctor
    Timer::paused = false;
    Timer::started = false;
    Timer::pauseTicks = 0;
    Timer::startTicks = 0;
}

BK::Timer::~Timer()
{
    //dtor
}

bool BK::Timer::IsPaused()
{
    return Timer::paused && Timer::started;
}

bool BK::Timer::IsStarted()
{
    return Timer::started;
}

int BK::Timer::GetTicks()
{
    int time = 0;

    if( started )
    {
        if( paused )
            time = pauseTicks;
        else
            time = glutGet(GLUT_ELAPSED_TIME) - Timer::startTicks;
    }

    return time;
}

int BK::Timer::Stop()
{
    started = false;
    paused = false;

	startTicks = 0;
	pauseTicks = 0;

	return started;
}

int BK::Timer::Unpause()
{
    if( started && paused )
    {
        paused = false;

        startTicks = glutGet(GLUT_ELAPSED_TIME) - pauseTicks;

        pauseTicks = 0;
    }

    return started && !paused;
}

int BK::Timer::Pause()
{
    if( started && !paused )
    {
        paused = true;

        pauseTicks = glutGet(GLUT_ELAPSED_TIME) - startTicks;
		startTicks = 0;
    }

    return started && paused;
}

int BK::Timer::Start()
{
    if(!started)
    {
        started = true;
        paused = false;

        startTicks = glutGet(GLUT_ELAPSED_TIME);
        pauseTicks = 0;
    }

    return started;
}


