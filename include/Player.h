#ifndef PLAYER_H_INCLUDED
#define PLAYER_H_INCLUDED

#include "GravityElement.h"
#include "Event.h"

using namespace BK;

class Player : public GravityElement
{
public:
    Player(string n, Object* p) : GravityElement(n,p) { }
    Error Event(BK::EventData*, float);
    Error Loop(float);
    bool OnCollision(Element* other, Point* startPos, Point* endPos);
};

#endif // PLAYER_H_INCLUDED
