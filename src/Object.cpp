#include "../include/Object.h"
#include <iostream>
#include <algorithm>

BK::Object::Object(string name)
{
    Object::initialized = false;
    Object::name = name;
    Object::parent = NULL;
    Object::children = new vector<Object*>();

    Object::SetRenderState(RENDERSTATE_RENDER);
    Object::SetObjectType(CLIP_OBJECT);
}

BK::Object::Object(string name, Object* parent) : Object(name)
{
    Object::parent = parent;

    if(parent && !parent->HasChild(this))
        Object::parent->AddChild(this);
}

BK::Object::Object(string name, vector<Object*> children) : Object(name)
{
    Object::children = &children;
}

BK::Object::Object(string name, Object* parent, vector<Object*> children) : Object(name, parent)
{
   Object::children = &children;
}


BK::Object::~Object()
{
    Object::ChangeParent(NULL);
    Object::ClearChildren();
    Object::name = "";
}





void BK::Object::SetRenderState(RenderState state)
{
    Object::renderState = state;
}

BK::RenderState BK::Object::GetRenderState()
{
    return Object::renderState;
}



BK::ObjectType BK::Object::GetObjectType()
{
    return Object::objectType;
}

void BK::Object::SetObjectType(ObjectType objectType)
{
    Object::objectType = objectType;
}





string BK::Object::GetName()
{
    return Object::name;
}

BK::Object* BK::Object::GetChild(string name)
{
    if(!Object::children)
        return NULL;
     for(unsigned int i = 0; i < Object::children->size(); i++)
        if(name == (*Object::children)[i]->GetName())
            return (*Object::children)[i];
    return NULL;
}

BK::Object* BK::Object::GetChild(int index)
{
    if(!Object::children)
        return NULL;
    if(index < 0 || index >= (int)Object::children->size())
        return NULL;

    return (*Object::children)[index];

}

BK::Object* BK::Object::GetParent()
{
    return Object::parent;
}




bool BK::Object::HasChild(Object* obj)
{
    if(!Object::children)
        return false;

    for(unsigned int i = 0; i < Object::children->size(); i++)
        if(obj == (*Object::children)[i])
            return true;
    return false;
}

bool BK::Object::AllowChild(Object* obj)
{
    return (Object::objectType << 1) == obj->GetObjectType();
}



BK::Error BK::Object::ActivateChild(string name)
{
    return Object::ActivateChildren({name});
}

BK::Error BK::Object::ActivateChildren()
{
    return Object::ActivateChildren({});
}

BK::Error BK::Object::ActivateChildren(vector<string> names)
{
    if(names.size() == 0)
    {
        if(!Object::children)
            return NO_CHILDREN;

        for(unsigned int i = 0; i < Object::children->size(); i++)
            (*Object::children)[i]->SetRenderState(RENDERSTATE_RENDER);

        return SUCCESS;
    }

    for(unsigned int i = 0; i < names.size(); i++)
    {
        Object* buff;
        if((buff = Object::GetChild(names[i])))
            buff->SetRenderState(RENDERSTATE_RENDER);
    }

    return SUCCESS;
}


BK::Error BK::Object::DeactivateChild(string name)
{
    return Object::DeactivateChildren({name});
}

BK::Error BK::Object::DeactivateChildren()
{
    return Object::DeactivateChildren({});
}
BK::Error BK::Object::DeactivateChildren(vector<string> names)
{
    if(names.size() == 0)
    {
        if(!Object::children)
            return NO_CHILDREN;

        for(unsigned int i = 0; i < Object::children->size(); i++)
            (*Object::children)[i]->Deactivate();

        return SUCCESS;
    }

    for(unsigned int i = 0; i < names.size(); i++)
    {
        Object* buff;

        if((buff = Object::GetChild(names[i])))
            buff->Deactivate();
    }

    return SUCCESS;
}


BK::Error BK::Object::ToggleChild(string name)
{
    return Object::ToggleChildren({name});
}

BK::Error BK::Object::ToggleChildren()
{
    return Object::ToggleChildren({});
}

void BK::Object::Activate()
{
    renderState = RENDERSTATE_RENDER;
}

void BK::Object::Deactivate()
{
    renderState = RENDERSTATE_IGNORE;
}

BK::Error BK::Object::ToggleChildren(vector<string> names)
{
    if(names.size() == 0)
    {
        if(!Object::children)
            return NO_CHILDREN;

        for(unsigned int i = 0; i < Object::children->size(); i++)
        {
            if((*Object::children)[i]->IsActive())
                (*Object::children)[i]->Deactivate();
            else
                (*Object::children)[i]->Activate();
        }

        return SUCCESS;
    }

    for(unsigned int i = 0; i < names.size(); i++)
    {
        Object* buff;
        if((buff = Object::GetChild(names[i])))
            buff->SetRenderState((buff->GetRenderState() == RENDERSTATE_RENDER ? RENDERSTATE_IGNORE : RENDERSTATE_RENDER));
    }

    return SUCCESS;
}




BK::Error BK::Object::Render()
{
    if(Object::GetRenderState() == RENDERSTATE_RENDER)
    {
        if(!Object::children)
            return NO_CHILDREN;
        for(unsigned int i = 0; i < Object::children->size(); i++)
        {
            if((*Object::children)[i]->GetRenderState() == RENDERSTATE_RENDER)
            {
                (*Object::children)[i]->Render();
            }
        }

        return SUCCESS;
    }

    return WRONG_RENDER_STATE;
}

BK::Error BK::Object::Loop(float frametime)
{
    if(Object::GetRenderState() == RENDERSTATE_RENDER || GetRenderState() == RENDERSTATE_INVISIBLE)
    {
        for(unsigned int i = 0; i < Object::children->size(); i++)
        {
            if((*Object::children)[i]->GetRenderState() == RENDERSTATE_RENDER || (*Object::children)[i]->GetRenderState() == RENDERSTATE_INVISIBLE)
                (*Object::children)[i]->Loop(frametime);
        }
    }

    return SUCCESS;
}

BK::Error BK::Object::Event(BK::EventData* event, float frametime)
{
    if(Object::GetRenderState() == RENDERSTATE_RENDER || GetRenderState() == RENDERSTATE_INVISIBLE)
    {
        for(unsigned int i = 0; i < Object::children->size(); i++)
        {
            if((*Object::children)[i]->GetRenderState() == RENDERSTATE_RENDER || (*Object::children)[i]->GetRenderState() == RENDERSTATE_INVISIBLE)
                (*Object::children)[i]->Event(event, frametime);
        }
    }

    return SUCCESS;
}

BK::Object* BK::Object::ChangeParent(Object* obj)
{
    if(obj != Object::parent)
    {
        if(Object::parent)
            Object::parent->RemoveChild(this);
        Object::parent = obj;

        if(obj)
            obj->AddChild(this);
    }

    return this;
}

BK::Object* BK::Object::GetFirstParent(ObjectType type)
{
    Object* temp = GetParent();
    while(temp && temp->GetObjectType() != type)
    {
        temp = temp->GetParent();
    }
    return temp;
}

BK::Error BK::Object::AddChild(Object* obj)
{
    return AddChild(obj, RENDERSTATE_RENDER);
}

BK::Error BK::Object::AddChild(Object* obj, BK::RenderState renderState)
{
    if(!Object::children)
        Object::children = new vector<Object*>();

    if(obj && !Object::HasChild(obj) /*&& Object::AllowChild(obj)*/)
    {
       obj->SetRenderState(renderState);
       Object::children->push_back(obj);
       obj->ChangeParent(this);
       ChildAdded(obj);
    }

    return SUCCESS;
}

BK::Error BK::Object::AddChildren(vector<Object*> objs)
{
    return AddChildren(objs, RENDERSTATE_RENDER);
}

BK::Error BK::Object::AddChildren(vector<Object*> objs, BK::RenderState renderState)
{
    return AddChildren(objs, vector<BK::RenderState>(objs.size(), renderState));
}

BK::Error BK::Object::AddChildren(vector<Object*> objs, vector<BK::RenderState> renderStates)
{
    if(objs.size() != renderStates.size())
        return SIZE_MISMATCH;
    for(unsigned int i = 0; i < objs.size(); i++)
    {
        AddChild(objs[i], renderStates[i]);
    }

    return SUCCESS;
}


BK::Error BK::Object::RemoveChild(Object* obj)
{
    if(!Object::children)
        return NO_CHILDREN;


    if(find(Object::children->begin(), Object::children->end(), (Object*)obj) != Object::children->end())
    {
        Object::children->erase(find(Object::children->begin(), Object::children->end(), obj));

        return SUCCESS;
    }

    return CHILD_OBJECT_NOT_FOUND;
}

BK::Error BK::Object::RemoveChild(string name)
{
    return Object::RemoveChild(Object::GetChild(name));
}


BK::Error BK::Object::DeleteChild(Object* obj)
{
    Error ret = Object::RemoveChild(obj);

    if(ret == SUCCESS)
        delete obj;

    return ret;
}

BK::Error BK::Object::DeleteChild(string name)
{
    return Object::DeleteChild(Object::GetChild(name));
}


BK::Error BK::Object::ClearChildren()
{
    if(Object::children == NULL)
        return NO_CHILDREN;

    for(vector<Object*>::iterator it = Object::children->begin(); it != Object::children->end();)
    {
//        cout<<"Delete: "<<(*it)->GetName()<<endl;
        delete (*it);
//        cout<<"Deleted"<<endl;
    }

    return SUCCESS;
}
