/**
    Element.h
    Purpose:
        Represents an Element which can be rendered.

    @author Christoph Kaupper
    @version 1.0
    @date 04.07.2014
*/


#ifndef ELEMENT_H
#define ELEMENT_H


#include "Point.h"
#include "Size.h"
#include "Rect.h"
#include "ElementFlags.h"

#include "Scene.h"
#include "Animation.h"

#include "Game.h"

#include "Object.h"
#include "FlipState.h"

namespace BK
{
    class Engine;
    class Scene;
    class Game;
    class Element : public Object
    {
        friend class Engine;
        friend class Scene;
        friend class Clip;
        public:

            Error Init(Point, int = 0, Size = Size{-1, -1},ElementFlags = FIXED_LOCATION);

            Point* GetLocation();

            void SetZIndex(float zindex);
            float GetZIndex(){ return zIndex; }


            void AddFlipState(FlipState flip);
            void RemoveFlipState(FlipState flip);
            void SetFlipState(FlipState flip);
            FlipState GetFlipState();
            void AddOpacity(float alpha);
            void SubtractOpacity(float alpha);
            void SetOpacity(float alpha);
            float GetOpacity();


            Size* GetSize() { return size; }
            void SetSize(Size* size) { delete Element::size; Element::size = size; }

            void ScaleTo(Size* size);

            Error ActivateAnimation(string name);

            void Move(float, float);
            void Move(Point*);
            void MoveTo(float, float);
            void MoveTo(Point*);

            void Rotate(float angle);
            void RotateTo(float angle);

        protected:
            Element(string name, Object* parent);
            bool CollidesWith(Element* other);
            // Events
            virtual void OnMove(Point*){};
            virtual bool OnCollision(Element* other, Point* startPos, Point* endPos){return true;}
            virtual void OnChildAdded(Object* obj) {}
            virtual ~Element();
        private:
            void MoveOutOf(Point* start, Element* other);
            Error Render();

            float zIndex;
            Point* location;
            Size* size;
            ElementFlags flags;


            float rotation;
            FlipState flipState;
            float opacity;

    };
}
#endif // ELEMENT_H
