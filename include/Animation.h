#ifndef ANIMATION_H
#define ANIMATION_H
/**
    Animation.h
    Purpose: Represents an animation consisting multiple textures (extracted from an animation file or added by yourself).

    @author Christoph B�hmwalder
    @version 1.0
    @date 05.07.2014
*/

#include "Object.h"
#include <iostream>
#include "Clip.h"

namespace BK
{
    class Animation final : public Object
    {
        friend class Engine;
        public:
            Error Init(float);

            /**
            Imports an animation file.

            @param The animation file to be loaded
            @return status code
            */
            Error ImportFromFile(string path);

            void SetDelta(float delta){ Animation::delta = delta <= 0 ? 1 : delta; }
            void Rotate(int angle);
            void RotateTo(int angle);
            Animation(string name, Object* parent);

        protected:

        private:
            virtual ~Animation();
            Error Loop(float frametime);
            void ChildAdded(Object* obj);
            int currentClip = 0;
            float delta = 0;
    };
}
#endif // ANIMATION_H
