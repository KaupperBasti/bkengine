#ifndef WALL_H
#define WALL_H

#include <Element.h>
#include "Point.h"

class Wall : public BK::Element
{
    public:
        Wall(string n, Object* o) : Element(n, o){}
        virtual ~Wall();
        virtual bool OnCollision(BK::Element* e, BK::Point* s, BK::Point* ep){}
    protected:
    private:
};

#endif // WALL_H
