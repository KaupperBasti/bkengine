#ifndef BKCOLOR_H
#define BKCOLOR_H

#include <stdint.h>

namespace BK
{
    class Color
    {
    public:
        Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
    private:
        uint8_t r;
        uint8_t g;
        uint8_t b;
        uint8_t a;
    };
}
#endif // BKCOLOR_H
