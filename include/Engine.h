#ifndef BKEngine_H
#define BKEngine_H


#include <iostream>
#include <vector>
#include <map>
#include <string>

using namespace std;

#include "Timer.h"
#include "Point.h"
#include "Rect.h"
#include "Error.h"


#include "Clip.h"
#include "Scene.h"
#include "Animation.h"
#include "Element.h"
#include "Game.h"
#include "Font.h"

namespace BK
{

    struct ClipData{
        string name;
        int textureID;
        float width;
        float height;
    };


    class Clip;
    class Animation;
    class Scene;
    class Element;

    class Engine final
    {
        friend class Game;
        friend class Clip;

        public:
            static Object* Get(string = "ENGINE", Object* = Engine::game);
            static vector<Object*> Get(ObjectType, Object* = Engine::game);

            static Game* CreateGame(string);
            static int RunGame();
            static void StopGame();

            template <class T> static T* Create(string n, Object* p){ return new T(n,p); }

        protected:
        private:
            static Game* game;
            static vector<ClipData*> textures;
    };
}
#endif // Engine_H
