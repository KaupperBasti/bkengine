/**
    Rect.h
    Purpose:
        Provides a Rectangle.

    @author Sebastian Kaupper
    @version 1.0
    @date 04.07.2014
*/


#ifndef RECT_H
#define RECT_H
namespace BK
{
    struct Rect{
        float x;
        float y;
        float w;
        float h;
    };
}
#endif // RECT_H
