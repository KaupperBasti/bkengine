/**
    Game.h
    Purpose:
        Provides an interface to control the Game logic.

    @author Sebastian Kaupper
    @version 1.0
    @date 04.07.2014
*/

#ifndef BKGame_H
#define BKGame_H


#include <iostream>
using namespace std;

#include "Object.h"
#include "GL/freeglut.h"
#include "GL/freeglut_ext.h"
#include "GL/freeglut_std.h"
#include "GL/glut.h"
#include "GL/gl.h"


namespace BK
{
    class Game final : public Object
    {
        friend class Clip;
        friend class Engine;
        friend class Scene;
        public:
            Error Init(int = 800, int = 600, string = "BK TEST WINDOW");



            Error ActivateScene(string name);

            Error Run();

        protected:
            static void DisplayFunc(void);
            static void Tick(int val);

            void Quit();

        private:
            Game(string);

            virtual ~Game();

            bool running;

            int windowHeight;
            int windowWidth;
            string windowTitle;


    };
}
#endif // Game_H
