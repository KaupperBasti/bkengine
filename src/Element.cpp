#include "../include/Element.h"

#include <cmath>
#include <algorithm>
#include <iostream>

BK::Element::Element(string name, Object* parent) : Object(name, parent)
{
    Element::objectType = ELEMENT_OBJECT;
    Element::rotation = 0;
    Element::opacity = 1;
    Element::flipState = FLIP_NONE;

    Element::flags = FIXED_LOCATION;
    Element::SetZIndex(0);
    Element::location = NULL;
    Element::size = NULL;

}

BK::Error BK::Element::Init(Point loc,  int zIndex, Size size, ElementFlags flags)
{
    Element::flags = flags;
    Element::SetZIndex(zIndex);
    Element::location = new Point{loc.x, loc.y};
    Element::size = new Size{size.w, size.h};

    return SUCCESS;
}


BK::Element::~Element()
{
    if(Element::location)
        delete Element::location;
    if(Element::size)
        delete Element::size;
}

BK::Point* BK::Element::GetLocation()
{
    if(Element::flags == FIXED_LOCATION)
        return location;

    if(!Element::parent || Element::parent->GetObjectType() != ELEMENT_OBJECT)
        return location;

    Point* p = ((Element*)Element::parent)->GetLocation();

    return new Point{p->x + Element::location->x, p->y + Element::location->y};
}

void BK::Element::SetZIndex(float zindex)
{
    ((Scene*)GetFirstParent(SCENE_OBJECT))->RefreshElementMap(this, zindex);

    Element::zIndex = zindex;
}

BK::Error BK::Element::ActivateAnimation(string name)
{
    Object* obj = GetChild(name);

    if(!obj)
        return CHILD_OBJECT_NOT_FOUND;

    DeactivateChildren();
    obj->Activate();
    return SUCCESS;
}


bool BK::Element::CollidesWith(Element* other)
{
    float x = GetLocation()->x;
    float y = GetLocation()->y;
    float w = GetSize()->w;
    float h = GetSize()->h;

    float otherx = other->GetLocation()->x;
    float othery = other->GetLocation()->y;
    float otherw = other->GetSize()->w;
    float otherh = other->GetSize()->h;

    if(other->GetRenderState() == RENDERSTATE_IGNORE)
        return false;

    // ------------- OBERKANTE --------------
    if(y >= othery && y <= othery + otherh)
    {
        if(x >= otherx && x <= otherx + otherw)
            return true;
        if(x + w >= otherx && x + w <= otherx + otherw)
            return true;
        if(x <= otherx && x + w >= otherx + otherw)
            return true;
        if(x >= otherx && x + w <= otherx + otherw)
            return true;
    }
    // ------------- UNTERKANTE ------------
    if(y + h >= othery && y + h <= othery + otherh)
    {
        if(x >= otherx && x <= otherx + otherw)
            return true;
        if(x + w >= otherx && x + w <= otherx + otherw)
            return true;
        if(x <= otherx && x + w >= otherx + otherw)
            return true;
        if(x >= otherx && x + w <= otherx + otherw)
            return true;

    }
    // ------------ LINKE KANTE -----------
    if(y<=othery && y+h >= othery+otherh)
    {
        if(x >= otherx && x <= otherx + otherw)
            return true;
        if(x + w >= otherx && x + w <= otherx + otherw)
            return true;
        if(x <= otherx && x + w >= otherx + otherw)
            return true;
        if(x >= otherx && x + w <= otherx + otherw)
            return true;
    }
    // ------------ RECHTE KANTE -----------
    if(y>=othery && y+h <= othery+otherh)
    {
        if(x >= otherx && x <= otherx + otherw)
            return true;
        if(x + w >= otherx && x + w <= otherx + otherw)
            return true;
        if(x <= otherx && x + w >= otherx + otherw)
            return true;
        if(x >= otherx && x + w <= otherx + otherw)
            return true;
    }
    return false;
}

void BK::Element::Move(Point* point)
{
    Element::Move(point->x, point->y);
}

typedef map<int, vector<BK::Element*>> MAP;
void BK::Element::Move(float x, float y)
{
    Point* startPos = new Point(*GetLocation());
    Point* endPos = new Point{startPos->x + x, startPos->y + y};

    MoveTo(endPos->x, endPos->y);


    Scene* scene = (Scene*)GetFirstParent(SCENE_OBJECT);
    Element* me = this;

    for(MAP::iterator it = scene->elementsInScene.begin(); it != scene->elementsInScene.end(); it++)
    {
        vector<Element*> v = (*it).second;
        for(unsigned int i = 0; i < v.size(); i++)
        {
            if(me->GetZIndex() == v[i]->GetZIndex() && me != v[i] && me->CollidesWith(v[i]))
            {
                if(!OnCollision(v[i], startPos, endPos))
                    MoveOutOf(startPos, v[i]);
            }
        }
    }

    for(int i = 0; i < children->size(); i++)
    {
        if(((*children)[i])->GetObjectType() == ELEMENT_OBJECT)
            ((Element*)((*children)[i]))->OnMove(new Point{x, y});
    }

    delete startPos;
    delete endPos;
}

void BK::Element::MoveOutOf(Point* start, Element* other)
{
    Point* actPos = new Point(*GetLocation());
    float dir = (signbit(actPos->y - start->y) == 0 ? 1 : -1);
    float abs = hypotf(actPos->x - start->x, actPos->y - start->y);

    float alpha = asinf((actPos->x - start->x)/abs);
    float movex =  sinf(alpha) * (0.1f);
    float movey =  cosf(alpha) * (0.1f);

    while(CollidesWith(other))
    {
        location->x -= movex * dir;
        location->y -= movey * dir;
    }

    delete actPos;
}

void BK::Element::MoveTo(float x, float y)
{
    Element::MoveTo(new Point{x,y});
}

void BK::Element::MoveTo(Point* point)
{
    OnMove(location);
    delete location;
    location = point;
}


void BK::Element::Rotate(float angle)
{
    rotation += angle;
    rotation = ((int)rotation) % 360;
}

void BK::Element::RotateTo(float angle)
{
    rotation = angle;
     rotation = ((int)rotation) % 360;
}


void BK::Element::ScaleTo(Size* size)
{
    float factorw = Element::size->w / size->w;
    float factorh = Element::size->h / size->h;

    for(unsigned int i = 0; i < Element::children->size();i++)
    {
        Object* temp = (*Element::children)[i];
        if(temp->GetObjectType() == ELEMENT_OBJECT)
        {
            Element* ele = (Element*)temp;
            ele->ScaleTo(new Size{ele->GetSize()->w / factorw, ele->GetSize()->h / factorh});
        }
    }

    SetSize(size);
}

void BK::Element::AddFlipState(FlipState flip)
{
    SetFlipState((FlipState)(GetFlipState() | flip));
}
void BK::Element::RemoveFlipState(FlipState flip)
{
    if(GetFlipState() & flip)
        SetFlipState((FlipState)(GetFlipState() ^ flip));
}
void BK::Element::SetFlipState(FlipState flip)
{
    if(parent->GetObjectType() == SCENE_OBJECT || flags == FIXED_LOCATION)
        flipState = flip;
    else
        ((Element*)parent)->SetFlipState(flip);
}
BK::FlipState BK::Element::GetFlipState()
{
    if(parent->GetObjectType() == SCENE_OBJECT || flags == FIXED_LOCATION)
        return flipState;

    return ((Element*)parent)->GetFlipState();
}

void BK::Element::AddOpacity(float alpha)
{
    SetOpacity(GetOpacity() + alpha);
}
void BK::Element::SubtractOpacity(float alpha)
{
    SetOpacity(GetOpacity() - alpha);
}
void BK::Element::SetOpacity(float alpha)
{
    if(parent->GetObjectType() == SCENE_OBJECT || flags == FIXED_LOCATION)
        opacity = (alpha < 0 ? 0 : (alpha > 1 ? 1 : alpha));
    else
        ((Element*)parent)->SetOpacity(alpha);
}
float BK::Element::GetOpacity()
{
    if(parent->GetObjectType() == SCENE_OBJECT || flags == FIXED_LOCATION)
        return opacity;

    return ((Element*)parent)->GetOpacity();
}


BK::Error BK::Element::Render()
{
    if(Element::GetRenderState() == RENDERSTATE_IGNORE)
        return WRONG_RENDER_STATE;
    if(!Element::children)
        return NO_CHILDREN;

    for(unsigned int i = 0; i < Element::children->size(); i++)
    {
        if((*Element::children)[i]->GetObjectType() != ELEMENT_OBJECT)
            (*Element::children)[i]->Render();
    }

    return SUCCESS;
}
