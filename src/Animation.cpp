#include "Animation.h"

BK::Animation::Animation(string name, Object* parent) : Object(name, parent)
{
    SetDelta(1);
    SetObjectType(ANIMATION_OBJECT);
}

BK::Error BK::Animation::Init(float delta)
{
    Object::initialized = true;
    Animation::SetDelta(delta);

    return SUCCESS;
}

BK::Animation::~Animation()
{

}

BK::Error BK::Animation::Loop(float frametime)
{
    static float frametimeCounter = 0;
    static int frameCount = 0;
    //cout << "loop " << frameCount << endl;
    frametimeCounter += frametime;
    //cout << frametimeCounter << " " << delta << endl;
    while(frametimeCounter > delta)
    {
        DeactivateChildren();
        if(children)
        {
            currentClip++;
            currentClip %= children->size();
            ActivateChild(GetChild(currentClip)->GetName());
        }
        frametimeCounter -= delta;
    }
    frameCount++;
    return Object::Loop(frametime);
}

void BK::Animation::ChildAdded(Object* obj)
{
    if(children->size() > 1)
        DeactivateChild(obj->GetName());
}
