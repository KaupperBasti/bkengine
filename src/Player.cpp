#include "Player.h"

using namespace BK;

Error Player::Loop(float frameTime)
{
    if(Event::IsPressed(KEY_RIGHT))
    {
        Player::Move(10,0);
        Player::ActivateAnimation("walking");
    }
    if(Event::IsPressed(KEY_LEFT))
    {
        Player::Move(-10,0);
        Player::ActivateAnimation("walking");

    }
    if(Event::IsPressed(KEY_UP))
        Player::Move(0,-13);
    if(Event::IsPressed(KEY_DOWN))
        Player::Move(0,10);
    if(Event::IsPressed(KEY_ZERO))
        Player::Rotate(20);
    if(Event::IsPressed(KEY_CTRL_RIGHT))
        Player::Rotate(-20);
        return GravityElement::Loop(frameTime);
}
Error Player::Event(BK::EventData* event, float frametime)
{
    if(event->type == KEYBOARD && event->pressed)
    {
        if(event->keyboardKey == KEY_LEFT)
            Player::AddFlipState(FLIP_VERTICAL);
        if(event->keyboardKey == KEY_RIGHT)
            Player::RemoveFlipState(FLIP_VERTICAL);

        if(event->keyboardKey == KEY_F)
        {
            Player::SetOpacity(Player::GetOpacity() - 0.1f);
        }

        if(event->keyboardKey == KEY_R)
        {
            Player::SetOpacity(Player::GetOpacity() + 0.1f);
        }
    }
    if(event->type == KEYBOARD && !event->pressed)
    {
        if(event->keyboardKey == KEY_LEFT || event->keyboardKey == KEY_RIGHT)
        {

            Player::ActivateAnimation("standing");
        }
    }
    return GravityElement::Event(event, frametime);

}

bool Player::OnCollision(Element* other, Point* startPos, Point* endPos)
{
    //other->Deactivate();
    GravityElement::OnCollision(other, startPos, endPos);
    return 0;
}
